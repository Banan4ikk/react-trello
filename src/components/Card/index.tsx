import React, {FunctionComponent, useEffect, useState} from 'react';
import {ICardProps} from "./interfaces";
import {Comments, CommentsCount, StyledCard} from "./styles";
import {ICommentsData} from "../../interfaces/interfaces";

const Card: FunctionComponent<ICardProps> = ({title, id, setCardId, getComments, comments}) => {

  const [localComments, setLocalComments] = useState<ICommentsData[]>(getComments(id, comments));

  useEffect(() => {
    setLocalComments(getComments(id, comments))
  }, [comments])

  const onCardClick = () => {
    setCardId(id)
  }

  return (
    <StyledCard onClick={onCardClick}>
      {title}
      {localComments.length > 0 ?
        <Comments>
          <img src={"img/comment.svg"}/>
          <CommentsCount>{localComments.length}</CommentsCount>
        </Comments>
        : null}
    </StyledCard>
  )
};

export default Card;
