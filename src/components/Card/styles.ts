import styled from "styled-components";

export const StyledCard = styled.div`
  background-color: #fff;
  width: auto;
  height: fit-content;
  margin-top: 10px;
  border-radius: 5px;
  box-shadow: 0 0 5px -2px #000000;
  padding: 10px;
  transition: background-color 0.15s ease-in-out;

  &:hover {
    background-color: #ccced9;
    cursor: pointer;
  }
`

export const Comments = styled.div`
  display: flex;
  width: fit-content;
  margin-top: 10px;
`

export const CommentsCount= styled.div`
  margin-left: 5px;
  transform: translateY(3px);

`