export interface IUserMenuProps {
  visible: boolean;
  authorName: string;
}