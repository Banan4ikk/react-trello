import styled from "styled-components";

export const StyledHeader = styled.header`
  position: fixed;
  z-index: 1000;
  height: 60px;
  width: 100%;
  background-color: #9c9c9c;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 20px;
`

export const StyledLogo = styled.div`
  z-index: 99999;
  height: 40px;
  transform: translateY(6px);
  &:hover{
    cursor: pointer;
  }
`

export const StyledUserIcon = styled.div`
  z-index: 99999;
  height: 40px;
  width: 40px;
  &:hover{
    cursor: pointer;
  }
`