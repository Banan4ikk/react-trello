import React, {FunctionComponent, useState} from 'react';
import {StyledHeader, StyledLogo, StyledUserIcon} from './styles'
import UserMenu from "../UserMenu";
import AuthorModal from "../AuthorModal";
import {IHeaderProps} from "./interfaces";

const Header: FunctionComponent<IHeaderProps> = ({authName, setAuthName}) => {

  const [visibleUser, setVisibleUser] = useState(false);

  const onUserClick = () => {
    setVisibleUser(!visibleUser)
  }

  return (
    <StyledHeader>
      <StyledLogo>
        <img src="img/trello-logo.svg"/>
      </StyledLogo>

      <StyledUserIcon>
        <img src="img/user-icon.svg" onClick={onUserClick}/>
      </StyledUserIcon>
      <AuthorModal authorName={authName} setAuthName={setAuthName}/>
      <UserMenu visible={visibleUser} authorName={authName}/>
    </StyledHeader>
  )
};

export default Header;
