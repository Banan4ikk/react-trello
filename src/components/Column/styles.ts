import styled from "styled-components";

export const StyledColumn = styled.div`
  &:first-child {
    margin-left: 0;
  }

  margin-top: 20px;
  height: 90vh;
  width: 300px;
  background: #ebecf0;
  border-radius: 5px;
  justify-content: center;
  flex-direction: column;
  padding: 10px;
  margin-left: 30px;
`

export const ColumnHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  background: #b1b5c4;
  width: auto;
  height: 50px;
  font-weight: 600;
  font-size: 1.5em;
  &:hover{
    cursor: pointer;
  }
`

export const AddCard = styled.div`
  //  TODO: улучшить
  padding: 10px;
  display: flex;
  border: 1px #939292FF solid;
  border-radius: 10px;
  justify-content: space-between;
  align-items: center;
  position: relative;
  bottom: -20px;
  height: auto;
  width: 100%;
`

export const AddCardText = styled.div`
  font-size: 1.15em;
`

export const StyledImg = styled.img`
  height: 30px;
  transition: background-color 0.1s ease-in-out;
  
  &:hover{
    background-color: #ccced9;
    cursor: pointer;
  }
`
