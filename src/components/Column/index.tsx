import React, {FunctionComponent, useEffect, useState} from 'react';
import {IColumnProps} from "./interfaces";
import {AddCard, AddCardText, ColumnHeader, StyledColumn, StyledImg} from "./styles";
import Card from "../Card";
import {ICard} from "../../interfaces/interfaces";
import EditComponent from "../EditComponent";

const Column: FunctionComponent<IColumnProps> = ({
                                                   colId,
                                                   title,
                                                   setCardId,
                                                   changeTitle,
                                                   cards,
                                                   getCards,
                                                   addCard,
                                                   comments,
                                                   getCommentsById
                                                 }) => {

  const [inputValue, setInputValue] = useState('');
  const [isEdit, setIsEdit] = useState(false);
  const [localCards, setLocalCards] = useState<ICard[]>(() => getCards(colId, cards));

  useEffect(() => {
    setLocalCards(() => getCards(colId, cards))
  }, [cards])

  const onAddCard = () => {
    addCard(colId)
  }

  const onClickTitle = () => {
    setIsEdit(true)
  }

  const onEditInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value)
  }

  const onSubmitEdit = (event: React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    changeTitle(colId, inputValue)
    setIsEdit(false)
    resetInputValue()
  }

  const resetInputValue = () => {
    setInputValue('')
  }

  return (
    <StyledColumn>
      {isEdit ? <EditComponent
          onSubmitForm={onSubmitEdit}
          onEditInput={onEditInput}
          setIsEdit={setIsEdit}
          inputValue={inputValue}
        /> :
        <ColumnHeader onClick={onClickTitle}>{title}</ColumnHeader>}
      {
        localCards.map((item) =>
          <Card
            key={item.id}
            id={item.id}
            title={item.title}
            setCardId={setCardId}
            getComments={getCommentsById}
            comments={comments}
          />)
      }
      <AddCard>
        <AddCardText>
          Add card
        </AddCardText>
        <StyledImg src="img/plus-svg.svg" onClick={onAddCard}/>
      </AddCard>
    </StyledColumn>
  )
};

export default Column;
