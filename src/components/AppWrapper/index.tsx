import React, {FunctionComponent} from 'react';
import {StyledAppWrapper} from './styles';

const AppWrapper: FunctionComponent = ({children}) => {
  return (
    <StyledAppWrapper>
      {children}
    </StyledAppWrapper>
  )
}

export default AppWrapper;