import styled from "styled-components";

export const StyledContentBlock = styled.div`
  position: fixed;
  display: flex;
  justify-content: flex-start;
  height: 100vh;
  width: 100%;
  background-color: #fff;
  padding: 50px 20px;
`