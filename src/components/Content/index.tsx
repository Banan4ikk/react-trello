import React, {FunctionComponent, useState} from 'react';
import {StyledContentBlock} from './styles';
import Column from "../Column";
import ModalCard from "../ModalCard";
import {ICard, IColumnData, ICommentsData} from "../../interfaces/interfaces";
import {LOCALSTORAGE_KEYS} from "../../constants";
import {initialColumnsState} from "../../data";
import {IContentProps} from "./interfaces";

const Content: FunctionComponent<IContentProps> = ({authName}) => {

  const [cardId, setCardId] = useState<number | undefined>(undefined);

  const localColumns = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEYS.columns) as string)
  const [columns, setColumns] = useState<IColumnData[]>(localColumns === null ? initialColumnsState : localColumns);
  localStorage.setItem(LOCALSTORAGE_KEYS.columns, JSON.stringify(columns))

  const localComments = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEYS.comments) as string)
  const [comments, setComments] = useState<ICommentsData[]>(localComments === null ? [] : localComments);
  localStorage.setItem(LOCALSTORAGE_KEYS.comments, JSON.stringify(localComments))

  const localCards = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEYS.cards) as string)
  const [cards, setCards] = useState<ICard[]>(localCards === null ? [] : localCards);
  localStorage.setItem(LOCALSTORAGE_KEYS.cards, JSON.stringify(localCards))

  const changeTitle = (id: number, title: string) => {
    const newObj = columns.map((item) => {
      if (item.columnId === id) {
        item.title = title
      }
      return item
    })
    setColumns(newObj)
    localStorage.setItem('columns', JSON.stringify(newObj))
  }

  const changeTitleCard = (id: number, title: string) => {
    const newObjData = cards.map((item) => {
      if (item.id === id) {
        item.title = title
      }
      return item
    })
    setCards(newObjData)
    localStorage.setItem(LOCALSTORAGE_KEYS.cards, JSON.stringify(newObjData))

    const newObjCards = cards.map((item) => {
      if (item.id === id) {
        item.title = title
      }
      return item
    })
    setCards(newObjCards)
    localStorage.setItem(LOCALSTORAGE_KEYS.cards, JSON.stringify(newObjCards))
  }

  const changeCardData = (id: number, newData: string) => {
    const newObj = cards.map((item) => {
      if (item.id === id) {
        item.data = newData
      }
      return item
    })

    setCards(newObj)
    localStorage.setItem(LOCALSTORAGE_KEYS.cards, JSON.stringify(newObj))
  }

  const deleteCard = (id: number) => {
    const itemToDelete = cards.find(item => item.id === id)
    const newObj = cards.filter(item => item !== itemToDelete)

    setCards(newObj)
    localStorage.setItem(LOCALSTORAGE_KEYS.cards, JSON.stringify(newObj))
  }

  const changeComment = (id: number, newComment: string) => {
    const newObj = comments.map((item) => {
      if (item.id === id) {
        item.comment = newComment
      }
      return item
    })

    setComments(newObj)
    localStorage.setItem('comments', JSON.stringify(newObj))
  }

  const deleteComment = (id: number) => {
    const itemToDelete = comments.find(item => item.id === id)
    const newObj = comments.filter(item => item !== itemToDelete)

    setComments(newObj)
    localStorage.setItem('comments', JSON.stringify(newObj))
  }

  const addCard = (colId: number) => {
    const newObj: ICard = {
      id: cards.length + 1,
      columnId: colId,
      title: "default title",
      data: "",
      author: authName
    }

    setCards([...cards, newObj])
    localStorage.setItem(LOCALSTORAGE_KEYS.cards, JSON.stringify([...cards, newObj]))
  }

  const addComment = (cardId: number, comment: string) => {
    const newComment: ICommentsData = {
      id: new Date().getMilliseconds(),
      cardId: cardId,
      name: authName,
      comment: comment
    }

    setComments(prevState => [...prevState, newComment])
    localStorage.setItem('comments', JSON.stringify([...comments, newComment]))
  }

  const getCommentsById = (id: number, comments: ICommentsData[]) => {
    return comments.filter((item) => item.cardId === id)
  }

  const getCardsById = (id: number, cards: ICard[]) => {
    return cards.filter((item) => item.columnId === id)
  }

  const getCardsDataById = (id: number, cards: ICard[]) => {
    return cards.filter((item) => item.id === id)
  }

  return (
    <>
      <StyledContentBlock>
        {columns.map((item) =>
          <Column
            key={item.columnId}
            title={item.title}
            colId={item.columnId}
            cards={cards}
            setCardId={setCardId}
            changeTitle={changeTitle}
            getCards={getCardsById}
            addCard={addCard}
            comments={comments}
            getCommentsById={getCommentsById}
          />
        )}
      </StyledContentBlock>
      {cardId &&
      <ModalCard
        cardId={cardId}
        setId={setCardId}
        cards={cards}
        comments={comments}
        columns={columns}
        getComments={getCommentsById}
        getCardData={getCardsDataById}
        changeTitle={changeTitleCard}
        changeData={changeCardData}
        deleteCard={deleteCard}
        changeComment={changeComment}
        deleteComment={deleteComment}
        addComment={addComment}
      />}
    </>
  )
};

export default Content;