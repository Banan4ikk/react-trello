import React, {useState} from 'react';
import AppWrapper from "./components/AppWrapper";
import Header from "./components/Header";
import Content from "./components/Content";
import {useCookies} from "react-cookie";

function App() {

  const [cookies] = useCookies(['authName'])
  const [authName, setAuthName] = useState(cookies.authName);

  return (
    <AppWrapper>
      <Header
        authName={authName}
        setAuthName={setAuthName}
      />
      <Content
        authName={authName}
      />
    </AppWrapper>
  );
}

export default App;
